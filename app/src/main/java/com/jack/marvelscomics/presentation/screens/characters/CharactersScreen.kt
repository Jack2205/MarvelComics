package com.jack.marvelscomics.presentation.screens.characters

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import com.jack.marvelscomics.presentation.screens.characters.components.CharacterViewNoItems
import com.jack.marvelscomics.presentation.screens.characters.components.CharactersListDisplay
import com.jack.marvelscomics.presentation.screens.characters.components.CharactersViewError
import com.jack.marvelscomics.presentation.screens.characters.components.ViewLoading

@Composable
fun CharactersRoute(
    modifier: Modifier = Modifier,
    viewModel: CharactersViewModel = hiltViewModel(),
    onDetailsRoute: (Int?) -> Unit
) {

    CharactersScreen(
        modifier = modifier,
        state = viewModel.viewState.value,
        sendEvent = { viewModel.sendEvent(it) },
        onDetailsRoute = { onDetailsRoute(it) }
    )
}

@Composable
fun CharactersScreen(
    modifier: Modifier = Modifier,
    state: CharactersViewState,
    sendEvent: (CharactersEvents) -> Unit,
    onDetailsRoute: (Int?) -> Unit
) {

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        when (state.screenState) {
            CharactersScreenState.Loading -> ViewLoading()
            CharactersScreenState.Error -> CharactersViewError { sendEvent(CharactersEvents.Fetch) }
            CharactersScreenState.Success -> CharactersListDisplay(
                modifier = modifier,
                charactersList = state.data
            ) { onDetailsRoute(it) }
            CharactersScreenState.NoneData -> CharacterViewNoItems()
        }
    }

}