package com.jack.marvelscomics.presentation.screens.character_details

import com.jack.marvelscomics.domain.models.Comic
import com.jack.marvelscomics.domain.models.MarvelCharacter
import com.jack.marvelscomics.presentation.base.ViewEffect
import com.jack.marvelscomics.presentation.base.ViewEvent
import com.jack.marvelscomics.presentation.base.ViewState

data class CharactersDetailsViewState(
    val totalScreenData: Pair<MarvelCharacter?, List<Comic>?>?,
    val screenState: CharactersDetailsScreenState
): ViewState
sealed class CharactersDetailsScreenState {
    data object Loading: CharactersDetailsScreenState()
    data object Success: CharactersDetailsScreenState()
    data object NoneData: CharactersDetailsScreenState()
    data object Error: CharactersDetailsScreenState()
}

sealed class CharactersDetailsEvents: ViewEvent {
    data object FetchCharacterById: CharactersDetailsEvents()
}

sealed class CharactersDetailsEffects: ViewEffect