package com.jack.marvelscomics.presentation.screens.characters.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.jack.marvelscomics.domain.models.MarvelCharacter


@Composable
fun CharactersListDisplay(
    modifier: Modifier = Modifier,
    charactersList: List<MarvelCharacter>,
    onDetailsRoute: (Int?) -> Unit,
) {
    val listState = rememberLazyGridState()

    Box(modifier = modifier.fillMaxSize()) {
        LazyVerticalGrid(
            modifier = Modifier.fillMaxSize(),
            columns = GridCells.Fixed(count = 2),
            state = listState
        ) {
            charactersList.forEach { cardItem ->
                item {
                    CharacterCardItem(
                        model = cardItem,
                        onItemClick = { id ->
                            onDetailsRoute(id)
                        }
                    )
                }
            }
        }
    }
}