package com.jack.marvelscomics.presentation.screens.character_details.navigation

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.jack.marvelscomics.presentation.screens.character_details.CharacterDetailsRoute

const val characterId = "characterId"
const val characterDetailsRouteName = "character_details_route"
const val characterDetailsRoute = "$characterDetailsRouteName/{$characterId}"

fun NavController.navigateToCharacterDetailsScreen(
    characterId: Int?,
    navOptions: NavOptions? = null
) {
    this.navigate("$characterDetailsRouteName/$characterId", navOptions)
}

fun NavGraphBuilder.characterDetailsScreen(
    onBackClick: () -> Unit,
) {
    composable(
        route = characterDetailsRoute,
        arguments = listOf(navArgument(characterId) { type = NavType.IntType })
    ) {
        CharacterDetailsRoute {
            onBackClick()
        }
    }
}