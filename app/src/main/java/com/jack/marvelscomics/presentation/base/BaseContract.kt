package com.jack.marvelscomics.presentation.base

/**
 * Для каждого экрана (активность или фрагмент) должен создаваться файл контракта,
 * содержащий все Состояние, Действия и Эффекты
 */

interface ViewState

interface ViewEvent

interface ViewEffect