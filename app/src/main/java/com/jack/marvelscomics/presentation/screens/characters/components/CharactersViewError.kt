package com.jack.marvelscomics.presentation.screens.characters.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.jack.marvelscomics.ui.theme.MainTheme
import com.jack.marvelscomics.R

@Composable
fun CharactersViewError(
    onReloadClick: () -> Unit
) {

    Box(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .align(Alignment.Center),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Icon(
                modifier = Modifier.size(96.dp),
                imageVector = Icons.Filled.Warning,
                tint = MaterialTheme.colorScheme.surfaceTint,
                contentDescription = "Error loading items"
            )

            Text(
                modifier = Modifier.padding(top = 16.dp, bottom = 24.dp),
                text = stringResource(id = R.string.error_loading),
                textAlign = TextAlign.Center,
                color = MaterialTheme.colorScheme.error
            )

            Button(
                modifier = Modifier.fillMaxWidth(),
                onClick = onReloadClick,
            ) {
                Text(
                    text = stringResource(id = R.string.action_refresh),
                )
            }
        }
    }
}

@Composable
@Preview
fun DailyViewError_Preview() {
    MainTheme(darkTheme = true) {
        CharactersViewError(onReloadClick = {})
    }
}