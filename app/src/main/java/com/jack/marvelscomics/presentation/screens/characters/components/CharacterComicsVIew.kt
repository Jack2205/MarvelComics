package com.jack.marvelscomics.presentation.screens.characters.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.jack.marvelscomics.R
import com.jack.marvelscomics.presentation.screens.character_details.CharactersDetailsScreenState
import com.jack.marvelscomics.presentation.screens.character_details.CharactersDetailsViewState


@Composable
fun CharacterComicsView(
    state: CharactersDetailsViewState,
    onComicsClick: (Int) -> Unit
) {
    when(state.screenState) {
        CharactersDetailsScreenState.Error -> {
            Box(modifier = Modifier.fillMaxWidth()) {
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(alignment = Alignment.Center)
                        .padding(horizontal = 16.dp),
                    text = stringResource(id = R.string.error_loading_comics),
                    style = MaterialTheme.typography.bodySmall
                )
            }

        }
        CharactersDetailsScreenState.Success -> {
            Column(
                modifier = Modifier.fillMaxSize()
            ) {
                if (state.totalScreenData?.second?.isNotEmpty() == true) {
                    Text(
                        text = stringResource(R.string.comics_bottom_navigation_title),
                        style = MaterialTheme.typography.headlineMedium,
                        modifier = Modifier.padding(horizontal = 16.dp, vertical = 2.dp)
                    )

                    LazyRow (
                        contentPadding = PaddingValues(4.dp),
                        horizontalArrangement = Arrangement.spacedBy(4.dp),
                        modifier = Modifier.fillMaxWidth().height(320.dp)
                    ) {
                        state.totalScreenData.second?.forEach { comic ->
                            item {
                                ComicCardItem(
                                    model = comic,
                                    onItemClick = {
                                        comic.id?.let { onComicsClick(it) }
                                    }
                                )
                            }
                        }
                    }
                } else {
                    Box(modifier = Modifier.fillMaxWidth()) {
                        Text(
                            modifier = Modifier
                                .fillMaxWidth()
                                .align(alignment = Alignment.Center)
                                .padding(horizontal = 16.dp),
                            text = stringResource(id = R.string.error_loading_comics),
                            style = MaterialTheme.typography.bodySmall
                        )
                    }
                }
            }

        }

        else -> {}
    }
}