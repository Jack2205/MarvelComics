package com.jack.marvelscomics.presentation.screens.characters

import com.jack.marvelscomics.domain.models.MarvelCharacter
import com.jack.marvelscomics.presentation.base.ViewEffect
import com.jack.marvelscomics.presentation.base.ViewEvent
import com.jack.marvelscomics.presentation.base.ViewState

data class CharactersViewState(
    val data: List<MarvelCharacter>,
    val screenState: CharactersScreenState
): ViewState

sealed class CharactersScreenState {
    data object Loading: CharactersScreenState()
    data object Success: CharactersScreenState()
    data object NoneData: CharactersScreenState()
    data object Error: CharactersScreenState()
}

sealed class CharactersEvents: ViewEvent {
    data object Fetch: CharactersEvents()
}

sealed class CharactersEffects: ViewEffect