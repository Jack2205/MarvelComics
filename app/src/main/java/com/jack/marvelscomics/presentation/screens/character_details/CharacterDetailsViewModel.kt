package com.jack.marvelscomics.presentation.screens.character_details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.jack.marvelscomics.domain.repositories.CharactersRepository
import com.jack.marvelscomics.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CharacterDetailsViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val charactersRepository: CharactersRepository
): BaseViewModel<CharactersDetailsViewState, CharactersDetailsEvents, CharactersDetailsEffects>() {

    private val characterId: Int = checkNotNull(savedStateHandle["characterId"])

    init {
        fetch(characterId)
    }

    override val logTag: String
        get() = javaClass.simpleName

    override fun setInitialState() = CharactersDetailsViewState(
        totalScreenData = null,
        screenState = CharactersDetailsScreenState.Loading,
    )

    override fun handleEvents(event: CharactersDetailsEvents) {

    }

    private fun fetch(characterId: Int) {
        viewModelScope.launch {
            charactersRepository.getCharacterById(characterId)
                .combine(charactersRepository.getCharacterComicsById(characterId = characterId)) { characters, comics ->
                    Pair(characters.firstOrNull(), comics)
            }
            .onStart { setState { this.copy(screenState = CharactersDetailsScreenState.Loading) } }
            .catch { setState { this.copy(screenState = CharactersDetailsScreenState.Error) } }
            .collectLatest { result ->
                if (result.first != null || result.second?.isNotEmpty() == true) {
                    setState {
                        this.copy(
                            totalScreenData = result,
                            screenState = CharactersDetailsScreenState.Success
                        )
                    }
                } else {
                    setState { this.copy(screenState = CharactersDetailsScreenState.NoneData) }
                }

            }
        }
    }
}