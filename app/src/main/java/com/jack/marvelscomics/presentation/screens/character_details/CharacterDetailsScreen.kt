package com.jack.marvelscomics.presentation.screens.character_details

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import com.jack.marvelscomics.presentation.screens.characters.components.*
import com.jack.marvelscomics.ui.components.TopToolBar

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CharacterDetailsRoute(
    viewModel: CharacterDetailsViewModel = hiltViewModel(),
    onBackClick: () -> Unit
) {
    val state = viewModel.viewState.value
    CharacterDetailsScreen(state, { viewModel.sendEvent(it) }) {
        onBackClick()
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CharacterDetailsScreen(
    state: CharactersDetailsViewState,
    sendEvent: (CharactersDetailsEvents) -> Unit,
    onBackClick: () -> Unit,
) {

    Column(modifier = Modifier.fillMaxSize()) {
        TopToolBar {
            onBackClick()
        }
        when (state.screenState) {
            CharactersDetailsScreenState.Error -> {
                CharactersViewError {
                    sendEvent(CharactersDetailsEvents.FetchCharacterById)
                }
            }

            CharactersDetailsScreenState.Loading -> {
                ViewLoading()
            }

            CharactersDetailsScreenState.NoneData -> {
                CharacterViewNoItems()
            }

            CharactersDetailsScreenState.Success -> {
                state.totalScreenData?.first?.let { character ->
                    Column(
                        Modifier
                            .fillMaxSize()
                            .verticalScroll(rememberScrollState())
                    ) {
                        CharacterDetailsContent(character = character)
                        CharacterComicsView(state = state) {

                        }
                    }
                } ?: run {
                    CharacterViewNoItems()
                }
            }
        }
    }
}