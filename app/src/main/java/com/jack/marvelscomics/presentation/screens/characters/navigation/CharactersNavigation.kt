package com.jack.marvelscomics.presentation.screens.characters.navigation

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.jack.marvelscomics.presentation.navigation.NavigationDestination
import com.jack.marvelscomics.presentation.screens.character_details.navigation.navigateToCharacterDetailsScreen
import com.jack.marvelscomics.presentation.screens.characters.CharactersRoute

const val CHARACTERS_GRAPH_ROUTE_PATTERN = "characters_graph"
const val charactersRoute = "characters_route"

fun NavController.navigateToCharactersGraph(navOptions: NavOptions? = null) {
    this.navigate(CHARACTERS_GRAPH_ROUTE_PATTERN, navOptions)
}


fun NavGraphBuilder.charactersGraph(
    navController: NavController,
    nestedGraphs: NavGraphBuilder.() -> Unit,
) {
    navigation(
        route = CHARACTERS_GRAPH_ROUTE_PATTERN,
        startDestination = charactersRoute
    ) {
        composable(route = charactersRoute) {
            CharactersRoute { characterId ->
                navController.navigateToCharacterDetailsScreen(characterId)
            }
        }

        nestedGraphs()
    }
}