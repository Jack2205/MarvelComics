package com.jack.marvelscomics.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.jack.marvelscomics.presentation.screens.character_details.navigation.characterDetailsScreen
import com.jack.marvelscomics.presentation.screens.characters.navigation.CHARACTERS_GRAPH_ROUTE_PATTERN
import com.jack.marvelscomics.presentation.screens.comics.navigation.comicsGraph
import com.jack.marvelscomics.presentation.screens.characters.navigation.charactersGraph


@Composable
fun McNavHost(
    navController: NavHostController,
    modifier: Modifier = Modifier,
    startDestination: String = CHARACTERS_GRAPH_ROUTE_PATTERN
) {
    NavHost(
        navController = navController,
        startDestination = startDestination,
        modifier = modifier,
    ) {
        charactersGraph(navController) {
            characterDetailsScreen {
                navController.popBackStack()
            }
        }
        comicsGraph()
    }
}