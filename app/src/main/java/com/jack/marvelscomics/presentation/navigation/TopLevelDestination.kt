package com.jack.marvelscomics.presentation.navigation

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Face
import androidx.compose.material.icons.filled.Star
import androidx.compose.ui.graphics.vector.ImageVector
import com.jack.marvelscomics.R

enum class TopLevelDestination(
    val selectedIcon: ImageVector,
    val unselectedIcon: ImageVector,
    val iconTextId: Int,
    val titleTextId: Int,
) {
    CHARACTERS(
        selectedIcon = Icons.Filled.Face,
        unselectedIcon = Icons.Filled.Face,
        iconTextId = R.string.characters_bottom_navigation_title,
        titleTextId = R.string.characters_bottom_navigation_title
    ),
    COMICS(
        selectedIcon = Icons.Filled.Star,
        unselectedIcon = Icons.Filled.Star,
        iconTextId = R.string.comics_bottom_navigation_title,
        titleTextId = R.string.comics_bottom_navigation_title
    ),
}