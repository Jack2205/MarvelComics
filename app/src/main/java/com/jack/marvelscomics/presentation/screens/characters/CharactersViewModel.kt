package com.jack.marvelscomics.presentation.screens.characters


import androidx.lifecycle.viewModelScope
import com.jack.marvelscomics.domain.repositories.CharactersRepository
import com.jack.marvelscomics.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CharactersViewModel @Inject constructor(
    private val charactersRepository: CharactersRepository
): BaseViewModel<CharactersViewState, CharactersEvents, CharactersEffects>() {

    init {
        sendEvent(CharactersEvents.Fetch)
    }

    override val logTag: String
        get() = javaClass.simpleName

    override fun setInitialState() = CharactersViewState(
        data = emptyList(),
        screenState = CharactersScreenState.Loading
    )

    override fun handleEvents(event: CharactersEvents) {
        when(event) {
            CharactersEvents.Fetch -> {
                fetchCharacters()
            }
        }
    }


    private fun fetchCharacters() {
        viewModelScope.launch {
            charactersRepository.getAllCharacters()
                .onStart { setState { this.copy(screenState = CharactersScreenState.Loading) } }
                .catch { setState { this.copy(screenState = CharactersScreenState.Error) } }
                .collectLatest { result ->
                    result.let {
                        if (it.isEmpty()) {
                            setState { this.copy(screenState = CharactersScreenState.NoneData) }
                        } else {
                            setState { this.copy(screenState = CharactersScreenState.Success, data = it) }
                        }
                    }
                }
        }
    }
}