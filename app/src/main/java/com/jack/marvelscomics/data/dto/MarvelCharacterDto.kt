package com.jack.marvelscomics.data.dto

import kotlinx.serialization.Serializable

@Serializable
data class MarvelCharacterDto(
    val id: Int?,
    val name: String?,
    val description: String?,
    val modified: String?,
    val resourceURI: String?,
    val urls: List<UrlDto>?,
    val thumbnail: ImageDto?,
    val comics: ComicListDto?,
    val stories: StoryListDto?,
    val events: EventListDto?,
    val series: SeriesListDto?
)

@Serializable
data class UrlDto(
    val type: String?,
    val url: String?
)
@Serializable
data class ImageDto(
    val path: String?,
    val extension: String?
)

