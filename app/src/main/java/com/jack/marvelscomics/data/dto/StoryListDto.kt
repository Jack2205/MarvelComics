package com.jack.marvelscomics.data.dto

import kotlinx.serialization.Serializable

@Serializable
data class StoryListDto(
    val available: Int?,
    val returned: Int?,
    val collectionURI: String?,
    val items: List<StorySummaryDto>?
)
@Serializable
data class StorySummaryDto(
    val resourceURI: String?,
    val name: String?,
    val type: String?
)
