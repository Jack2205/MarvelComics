package com.jack.marvelscomics.data.dto

import kotlinx.serialization.Serializable

@Serializable
data class SeriesListDto(
    val available: Int?,
    val returned: Int?,
    val collectionURI: String?,
    val items: List<SeriesSummaryDto>?
)

@Serializable
data class SeriesSummaryDto(
    val resourceURI: String?,
    val name: String?
)
