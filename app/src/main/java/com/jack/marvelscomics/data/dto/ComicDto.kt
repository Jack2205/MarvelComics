package com.jack.marvelscomics.data.dto

import kotlinx.serialization.Serializable

@Serializable
data class ComicDto(
    val id: Int?,
    val digitalId: Int?,
    val title: String?,
    val issueNumber: Double?,
    val variantDescription: String?,
    val description: String?,
    /*@Serializable(DateSerializer::class) val modified: Date?,*/
    val isbn: String?,
    val upc: String?,
    val diamondCode: String?,
    val ean: String?,
    val issn: String?,
    val format: String?,
    val pageCount: Int?,
    val textObjects: List<TextObjectDto>?,
    val resourceUri: String? = null,
    val urls: List<UrlDto>?,
    val series: SeriesSummaryDto?,
    val variants: List<ComicSummaryDto>?,
    val collections: List<ComicSummaryDto>?,
    val collectedIssues: List<ComicSummaryDto>?,
    val dates: List<ComicDateDto>?,
    val prices: List<ComicPriceDto>?,
    val thumbnail: ImageDto?,
    val images: List<ImageDto>?,
    val creators: CreatorListDto?,
    val characters: CharacterListDto?,
    val stories: StoryListDto?,
    val events: EventListDto?
)

@Serializable
data class TextObjectDto(
    val type: String?,
    val language: String?,
    val text: String?
)

@Serializable
data class ComicDateDto(
    val type: String?,
    /*@Serializable(DateSerializer::class)  val date: Date?*/
)

@Serializable
data class ComicPriceDto(
    val type: String?,
    val price: Float?
)

@Serializable
data class CreatorListDto(
    val available: Int?,
    val returned: Int?,
    val collectionURI: String?,
    val items: List<CreatorSummaryDto>?
)

@Serializable
data class CreatorSummaryDto(
    val resourceURI: String?,
    val name: String?,
    val role: String?
)

@Serializable
data class CharacterListDto(
    val available: Int?,
    val returned: Int?,
    val collectionURI: String?,
    val items: List<CharacterSummaryDto>?
)

@Serializable
data class CharacterSummaryDto(
    val resourceURI: String?,
    val name: String?,
    val role: String? = null
)
