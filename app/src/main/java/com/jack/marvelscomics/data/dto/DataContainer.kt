package com.jack.marvelscomics.data.dto

import kotlinx.serialization.Serializable

@Serializable
data class DataContainer<T>(
    val offset: Int? = null,
    val limit: Int? = null,
    val total: Int? = null,
    val count: Int? = null,
    val results: T? = null
)
