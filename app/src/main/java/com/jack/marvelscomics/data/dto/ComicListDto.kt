package com.jack.marvelscomics.data.dto

import kotlinx.serialization.Serializable

@Serializable
data class ComicListDto(
    val available: Int?,
    val returned: Int?,
    val collectionURI: String?,
    val items: List<ComicSummaryDto>?
)
@Serializable
data class ComicSummaryDto(
    val resourceURI: String?,
    val name: String?
)
