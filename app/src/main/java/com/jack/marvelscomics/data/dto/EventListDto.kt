package com.jack.marvelscomics.data.dto

import kotlinx.serialization.Serializable

@Serializable
data class EventListDto(
    val available: Int?,
    val returned: Int?,
    val collectionURI: String?,
    val items: List<EventSummaryDto>?
)

@Serializable
data class EventSummaryDto(
    val resourceURI: String?,
    val name: String?
)
