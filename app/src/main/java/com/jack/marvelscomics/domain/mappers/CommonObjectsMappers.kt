package com.jack.marvelscomics.domain.mappers

import com.jack.marvelscomics.data.dto.CharacterListDto
import com.jack.marvelscomics.data.dto.CharacterSummaryDto
import com.jack.marvelscomics.data.dto.ComicDateDto
import com.jack.marvelscomics.data.dto.ComicPriceDto
import com.jack.marvelscomics.data.dto.ComicSummaryDto
import com.jack.marvelscomics.data.dto.CreatorListDto
import com.jack.marvelscomics.data.dto.CreatorSummaryDto
import com.jack.marvelscomics.data.dto.EventListDto
import com.jack.marvelscomics.data.dto.EventSummaryDto
import com.jack.marvelscomics.data.dto.ImageDto
import com.jack.marvelscomics.data.dto.SeriesSummaryDto
import com.jack.marvelscomics.data.dto.StoryListDto
import com.jack.marvelscomics.data.dto.StorySummaryDto
import com.jack.marvelscomics.data.dto.TextObjectDto
import com.jack.marvelscomics.data.dto.UrlDto
import com.jack.marvelscomics.domain.models.CharacterList
import com.jack.marvelscomics.domain.models.CharacterSummary
import com.jack.marvelscomics.domain.models.ComicDate
import com.jack.marvelscomics.domain.models.ComicPrice
import com.jack.marvelscomics.domain.models.ComicSummary
import com.jack.marvelscomics.domain.models.CreatorList
import com.jack.marvelscomics.domain.models.CreatorSummary
import com.jack.marvelscomics.domain.models.EventList
import com.jack.marvelscomics.domain.models.EventSummary
import com.jack.marvelscomics.domain.models.Image
import com.jack.marvelscomics.domain.models.SeriesSummary
import com.jack.marvelscomics.domain.models.StoryList
import com.jack.marvelscomics.domain.models.StorySummary
import com.jack.marvelscomics.domain.models.TextObject
import com.jack.marvelscomics.domain.models.Url

object CommonObjectsMappers {

    fun mapTextObjects(s: TextObjectDto) = TextObject(
        type = s.type.orEmpty(),
        language = s.language.orEmpty(),
        text = s.text
    )

    fun mapUrls(s: UrlDto): Url {
        return Url(
            url = s.url.orEmpty(),
            type = s.type
        )
    }

    fun mapSeriesSummary(s: SeriesSummaryDto?) = SeriesSummary(
        resourceURI = s?.resourceURI.orEmpty(),
        name = s?.name.orEmpty()
    )

    fun mapComicSummary(s: ComicSummaryDto) = ComicSummary(
        resourceURI = s.resourceURI.orEmpty(),
        name = s.name.orEmpty()
    )

    fun mapComicDate(s: ComicDateDto) = ComicDate(
        type = s.type.orEmpty()
    )

    fun mapComicPrice(s: ComicPriceDto) = ComicPrice(
        type = s.type.orEmpty(),
        price = s.price
    )

    fun mapImage(s: ImageDto?) = Image(
        path = s?.path.orEmpty(),
        extension = s?.extension.orEmpty()
    )

    fun mapCreatorList(s: CreatorListDto?) = CreatorList(
        available = s?.available ?: 0,
        returned = s?.returned ?: 0,
        collectionURI = s?.collectionURI.orEmpty(),
        items = s?.items?.map { mapCreatorSummary(it) }
    )

    fun mapCreatorSummary(s: CreatorSummaryDto) = CreatorSummary(
        resourceURI = s.resourceURI.orEmpty(),
        name = s.name.orEmpty(),
        role = s.role.orEmpty()
    )

    fun mapCharactersList(s: CharacterListDto?) = CharacterList(
        available = s?.available ?: 0,
        returned = s?.returned ?: 0,
        collectionURI = s?.collectionURI.orEmpty(),
        items = s?.items?.map { mapCharacterSummary(it) }
    )

    fun mapCharacterSummary(s: CharacterSummaryDto) = CharacterSummary(
        resourceURI = s.resourceURI.orEmpty(),
        name = s.name.orEmpty(),
        role = s.role.orEmpty()
    )

    fun mapStoriesList(s: StoryListDto?) = StoryList(
        available = s?.available ?: 0,
        returned = s?.returned ?: 0,
        collectionURI = s?.collectionURI.orEmpty(),
        items = s?.items?.map { mapStorySummary(it) }
    )

    fun mapStorySummary(s: StorySummaryDto) = StorySummary(
        resourceURI = s.resourceURI.orEmpty(),
        name = s.name.orEmpty(),
        type = s.type.orEmpty()
    )

    fun mapEventsList(s: EventListDto?) = EventList(
        available = s?.available ?: 0,
        returned = s?.returned ?: 0,
        collectionURI = s?.collectionURI.orEmpty(),
        items = s?.items?.map { mapEventSummary(it) }
    )

    fun mapEventSummary(s: EventSummaryDto) = EventSummary(
        resourceURI = s.resourceURI.orEmpty(),
        name = s.name.orEmpty()
    )
}