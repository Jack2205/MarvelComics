package com.jack.marvelscomics.domain.mappers

import com.jack.marvelscomics.data.dto.ComicListDto
import com.jack.marvelscomics.data.dto.ComicSummaryDto
import com.jack.marvelscomics.data.dto.EventListDto
import com.jack.marvelscomics.data.dto.ImageDto
import com.jack.marvelscomics.data.dto.MarvelCharacterDto
import com.jack.marvelscomics.data.dto.SeriesListDto
import com.jack.marvelscomics.data.dto.UrlDto
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapComicSummary
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapEventsList
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapImage
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapSeriesSummary
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapStoriesList
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapUrls
import com.jack.marvelscomics.domain.models.ComicSummary
import com.jack.marvelscomics.domain.models.ComicsList
import com.jack.marvelscomics.domain.models.EventList
import com.jack.marvelscomics.domain.models.Image
import com.jack.marvelscomics.domain.models.MarvelCharacter
import com.jack.marvelscomics.domain.models.SeriesList
import com.jack.marvelscomics.domain.models.Url
import java.util.Random

class CharactersMapper: Mapper<MarvelCharacterDto, MarvelCharacter> {

    override fun map(s: MarvelCharacterDto): MarvelCharacter {
        return MarvelCharacter(
            id = s.id ?: Random().nextInt(),
            name = s.name.orEmpty(),
            description = s.description.orEmpty(),
            modified = s.modified.orEmpty(),
            resourceURI = s.resourceURI.orEmpty(),
            urls = s.urls?.map { mapUrls(it) },
            thumbnail = mapImage(s.thumbnail),
            comics = mapComicsList(s.comics),
            stories = mapStoriesList(s.stories),
            events = mapEventsList(s.events),
            series = mapSeriesList(s.series)
        )
    }

    private fun mapComicsList(s: ComicListDto?) = ComicsList(
        available = s?.available ?: 0,
        returned = s?.returned ?: 0,
        collectionURI = s?.collectionURI.orEmpty(),
        items = s?.items?.map { mapComicSummary(it) }
    )

    private fun mapSeriesList(s: SeriesListDto?) = SeriesList(
        available = s?.available ?: 0,
        returned = s?.returned ?: 0,
        collectionURI = s?.collectionURI.orEmpty(),
        items = s?.items?.map { mapSeriesSummary(it) }
    )

}