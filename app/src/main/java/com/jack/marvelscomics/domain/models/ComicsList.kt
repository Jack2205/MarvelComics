package com.jack.marvelscomics.domain.models

data class ComicsList(
    val available: Int?,
    val returned: Int?,
    val collectionURI: String?,
    val items: List<ComicSummary>?
)
data class ComicSummary(
    val resourceURI: String?,
    val name: String?
)