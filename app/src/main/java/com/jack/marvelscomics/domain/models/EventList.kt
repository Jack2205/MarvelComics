package com.jack.marvelscomics.domain.models

import kotlinx.serialization.Serializable

data class EventList(
    val available: Int?,
    val returned: Int?,
    val collectionURI: String?,
    val items: List<EventSummary>?
)

data class EventSummary(
    val resourceURI: String?,
    val name: String?
)