package com.jack.marvelscomics.domain.repositories

import com.jack.marvelscomics.data.network.CharacterApiService
import com.jack.marvelscomics.domain.mappers.CharactersMapper
import com.jack.marvelscomics.domain.mappers.ComicsMapper
import com.jack.marvelscomics.domain.models.MarvelCharacter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class CharactersRepository @Inject constructor(
    private val characterApiService: CharacterApiService,
    private val charactersMapper: CharactersMapper,
    private val comicsMapper: ComicsMapper
) {
    fun getAllCharacters() = flow {
        val response = characterApiService.getAllCharacters().data?.results
        emit(charactersMapper.mapList(response.orEmpty()))
    }.flowOn(Dispatchers.IO)

    fun getCharacterById(characterId: Int) = flow {
        val response = characterApiService.getCharacterById(characterId = characterId).data?.results
        emit(charactersMapper.mapList(response.orEmpty()))
    }.flowOn(Dispatchers.IO)

    fun getCharacterComicsById(characterId: Int) = flow {
        val response = characterApiService.getCharacterComicsById(characterId).data?.results
        emit(comicsMapper.mapList(response.orEmpty()))
    }.flowOn(Dispatchers.IO)
}