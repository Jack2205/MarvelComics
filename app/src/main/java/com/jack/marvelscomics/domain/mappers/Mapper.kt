package com.jack.marvelscomics.domain.mappers

interface Mapper<Source, Target> {

    fun map(s: Source): Target

    fun mapNullable(s: Source?): Target? {
        return s?.let { map(s) }
    }

    fun mapList(sourceList: List<Source>): List<Target> {
        val result = mutableListOf<Target>()
        for (source in sourceList) {
            result.add(map(source))
        }
        return result
    }

    fun mapListNullable(sourceList: List<Source>?): List<Target> {
        if (sourceList == null) {
            return emptyList()
        }
        val result = mutableListOf<Target>()
        for (source in sourceList) {
            result.add(map(source))
        }
        return result
    }
}
