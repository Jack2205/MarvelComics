package com.jack.marvelscomics.domain.mappers

import com.jack.marvelscomics.data.dto.CharacterListDto
import com.jack.marvelscomics.data.dto.CharacterSummaryDto
import com.jack.marvelscomics.data.dto.ComicDateDto
import com.jack.marvelscomics.data.dto.ComicDto
import com.jack.marvelscomics.data.dto.ComicPriceDto
import com.jack.marvelscomics.data.dto.ComicSummaryDto
import com.jack.marvelscomics.data.dto.CreatorListDto
import com.jack.marvelscomics.data.dto.CreatorSummaryDto
import com.jack.marvelscomics.data.dto.EventListDto
import com.jack.marvelscomics.data.dto.EventSummaryDto
import com.jack.marvelscomics.data.dto.ImageDto
import com.jack.marvelscomics.data.dto.SeriesSummaryDto
import com.jack.marvelscomics.data.dto.StoryListDto
import com.jack.marvelscomics.data.dto.StorySummaryDto
import com.jack.marvelscomics.data.dto.TextObjectDto
import com.jack.marvelscomics.data.dto.UrlDto
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapCharactersList
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapComicDate
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapComicPrice
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapComicSummary
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapCreatorList
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapEventsList
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapImage
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapSeriesSummary
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapStoriesList
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapTextObjects
import com.jack.marvelscomics.domain.mappers.CommonObjectsMappers.mapUrls
import com.jack.marvelscomics.domain.models.Comic
import java.util.Random

class ComicsMapper: Mapper<ComicDto, Comic> {
    override fun map(s: ComicDto): Comic {
        return Comic(
            id = s.id ?: Random().nextInt(),
            digitalId = s.digitalId ?: Random().nextInt(),
            title = s.title.orEmpty(),
            issueNumber = s.issueNumber ?: 0.0,
            variantDescription = s.variantDescription.orEmpty(),
            description = s.description.orEmpty(),
            isbn = s.isbn.orEmpty(),
            upc = s.upc.orEmpty(),
            diamondCode = s.diamondCode.orEmpty(),
            ean = s.ean.orEmpty(),
            issn = s.issn.orEmpty(),
            format = s.format.orEmpty(),
            pageCount = s.pageCount ?: 0,
            textObjects = s.textObjects?.map { mapTextObjects(it) },
            resourceUri = s.resourceUri.orEmpty(),
            urls = s.urls?.map { mapUrls(it) },
            series = mapSeriesSummary(s.series),
            variants = s.variants?.map { mapComicSummary(it) },
            collections = s.collections?.map { mapComicSummary(it) },
            collectedIssues = s.collectedIssues?.map { mapComicSummary(it) },
            dates = s.dates?.map { mapComicDate(it) },
            prices = s.prices?.map { mapComicPrice(it) },
            thumbnail = mapImage(s.thumbnail),
            images = s.images?.map { mapImage(it) },
            creators = mapCreatorList(s.creators),
            characters = mapCharactersList(s.characters),
            stories = mapStoriesList(s.stories),
            events = mapEventsList(s.events)
        )
    }


}