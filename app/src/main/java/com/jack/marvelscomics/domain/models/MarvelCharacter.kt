package com.jack.marvelscomics.domain.models

data class MarvelCharacter(
    val id: Int?,
    val name: String?,
    val description: String?,
    val modified: String?,
    val resourceURI: String?,
    val urls: List<Url>?,
    val thumbnail: Image?,
    val comics: ComicsList?,
    val stories: StoryList?,
    val events: EventList?,
    val series: SeriesList?
)

data class Url(
    val type: String?,
    val url: String?
)

data class Image(
    val path: String?,
    val extension: String?
)