package com.jack.marvelscomics.di

import com.jack.marvelscomics.domain.mappers.CharactersMapper
import com.jack.marvelscomics.domain.mappers.ComicsMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object MappersModule {

    @Provides
    @ViewModelScoped
    fun provideCharactersMapper() = CharactersMapper()

    @Provides
    @ViewModelScoped
    fun provideComicsMapper() = ComicsMapper()
}