package com.jack.marvelscomics.di

import com.jack.marvelscomics.data.network.CharacterApiService
import com.jack.marvelscomics.domain.mappers.CharactersMapper
import com.jack.marvelscomics.domain.mappers.ComicsMapper
import com.jack.marvelscomics.domain.repositories.CharactersRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object RepositoriesModule {

    @Provides
    @ViewModelScoped
    fun provideCharactersRepository(
        characterApiService: CharacterApiService,
        charactersMapper: CharactersMapper,
        comicsMapper: ComicsMapper
    ) = CharactersRepository(characterApiService, charactersMapper, comicsMapper)
}