package com.jack.marvelscomics.ui

import androidx.compose.material3.windowsizeclass.WindowHeightSizeClass
import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.compose.ui.util.trace
import androidx.navigation.NavDestination
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navOptions
import com.jack.marvelscomics.presentation.navigation.TopLevelDestination
import com.jack.marvelscomics.presentation.screens.characters.navigation.charactersRoute
import com.jack.marvelscomics.presentation.screens.characters.navigation.navigateToCharactersGraph

@Composable
fun rememberMcAppState(
    windowSizeClass: WindowSizeClass,
    navController: NavHostController = rememberNavController()
): McAppState {
    return remember(navController, windowSizeClass) {
        McAppState(navController, windowSizeClass)
    }
}

@Stable
class McAppState(
    val navController: NavHostController,
    private val windowSizeClass: WindowSizeClass
) {
    val currentDestination: NavDestination?
        @Composable get() = navController
            .currentBackStackEntryAsState().value?.destination

    val currentTopLevelDestination: TopLevelDestination?
        @Composable get() = when (currentDestination?.route) {
            charactersRoute -> TopLevelDestination.CHARACTERS
            else -> null
        }

    val shouldShowBottomBar: Boolean
        get() = windowSizeClass.widthSizeClass == WindowWidthSizeClass.Compact

    /**
     * Top level destinations to be used in the BottomBar
     */
    val topLevelDestinations: List<TopLevelDestination> = TopLevelDestination.values().asList()

    fun navigateToTopLevelDestination(topLevelDestination: TopLevelDestination) {
        trace("Navigation: ${topLevelDestination.name}") {
            val topLevelNavOptions = navOptions {
                // Pop up to the start destination of the graph to
                // avoid building up a large stack of destinations
                // on the back stack as users select items
                popUpTo(navController.graph.findStartDestination().id) {
                    saveState = true
                }
                // Avoid multiple copies of the same destination when
                // reselecting the same item
                launchSingleTop = true
                // Restore state when reselecting a previously selected item
                restoreState = true
            }

            when (topLevelDestination) {
                TopLevelDestination.CHARACTERS -> navController.navigateToCharactersGraph(
                    topLevelNavOptions
                )

                TopLevelDestination.COMICS -> {}
            }
        }
    }
}

